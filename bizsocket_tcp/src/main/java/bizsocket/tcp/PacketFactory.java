package bizsocket.tcp;

import okio.BufferedSource;

import java.io.IOException;

/**
 * PacketFactory
 *
 * @since 2021-05-06
 */
public abstract class PacketFactory {
    private final PacketPool packetPool;

    public PacketFactory() {
        this(new DisabledPacketPool());
    }

    public PacketFactory(PacketPool packetPool) {
        this.packetPool = packetPool;
    }

    /**
     * create request packet with command and body
     *
     * @param request
     * @param reusable 可重复利用的packet
     * @return create request packet with command and body
     */
    public abstract Packet getRequestPacket(Packet reusable, Request request);

    /**
     * create heartbeat packet
     *
     * @param reusable 可重复利用的packet
     * @return create heartbeat packet
     */
    public abstract Packet getHeartBeatPacket(Packet reusable);

    /**
     * create packet from the stream of server
     *
     * @param reusable 可重复利用的packet
     * @param source
     * @return create packet from the stream of server
     * @throws IOException
     */
    public abstract Packet getRemotePacket(Packet reusable, BufferedSource source) throws IOException;

    public final Packet getRequestPacket(Request request) {
        Packet packet = getRequestPacket(getPacketPool().pull(), request);
        if (packet != null && request.recycleOnSend()) {
            packet.setFlags(packet.getFlags() | Packet.FLAG_AUTO_RECYCLE_ON_SEND_SUCCESS);
        }
        return packet;
    }

    public final Packet getHeartBeatPacket() {
        Packet packet = getHeartBeatPacket(getPacketPool().pull());
        if (packet != null) {

            //自动回收心跳包
            packet.setFlags(packet.getFlags() | Packet.FLAG_AUTO_RECYCLE_ON_SEND_SUCCESS);
        }
        return packet;
    }

    public final Packet getRemotePacket(BufferedSource source) throws IOException {
        return getRemotePacket(getPacketPool().pull(), source);
    }

    public PacketPool getPacketPool() {
        return packetPool;
    }
}
