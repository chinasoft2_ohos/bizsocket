package bizsocket.tcp;

/**
 * PacketPool
 *
 * @since 2021-05-06
 */
public interface PacketPool {
    Packet pull();

    void push(Packet packet);
}
