package bizsocket.tcp;

/**
 * Connection
 *
 * @since 2021-05-06
 */
public interface Connection {
    void connect() throws Exception;

    void disconnect();

    boolean isConnected();
}
