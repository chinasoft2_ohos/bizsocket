package bizsocket.tcp;

/**
 * DisabledPacketPool
 *
 * @since 2021-05-06
 */
public class DisabledPacketPool implements PacketPool {
    @Override
    public Packet pull() {
        return null;
    }

    @Override
    public void push(Packet packet) {

    }
}
