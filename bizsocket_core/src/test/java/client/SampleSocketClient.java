package client;

import bizsocket.core.*;
import bizsocket.logger.LoggerUtil;
import bizsocket.tcp.Packet;
import bizsocket.tcp.PacketFactory;
import bizsocket.tcp.Request;
import common.SampleCmd;
import common.SamplePacket;
import okio.BufferedSource;
import okio.ByteString;

import java.io.IOException;

/**
 * Created by tong on 16/10/3.
 */
public class SampleSocketClient extends AbstractBizSocket {
    public SampleSocketClient(Configuration configuration) {
        super(configuration);
    }

    @Override
    protected PacketFactory createPacketFactory() {
        return new WPBPacketFactory();
    }

    public static void main(String[] args) {
        SampleSocketClient client = new SampleSocketClient(new Configuration.Builder()
                .host("127.0.0.1")
                .port(9103)
                .build());
        try {
            client.connect();
        } catch (Exception e) {
            e.fillInStackTrace();
        }


        String json = "{\"productId\" : \"1\",\"isJuan\" : \"0\",\"type\" : \"2\",\"sl\" : \"1\"}";

        client.getSocketConnection().startHeartBeat();
        json = "{\"pageSize\" : \"10000\"}";
        client.request(new Request.Builder().command(SampleCmd.QUERY_ORDER_LIST.getValue()).utf8body(json).build(), new ResponseHandler() {
            @Override
            public void sendSuccessMessage(int command, ByteString requestBody, Packet responsePacket) {
                LoggerUtil.i("订单列表请求cmd: " + command + " ,requestBody: " + requestBody + " responsePacket: " + responsePacket);
            }

            @Override
            public void sendFailureMessage(int command, Throwable error) {
                LoggerUtil.i(command + " ,err: " + error);
            }
        });

        while (true) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.fillInStackTrace();
            }
        }
    }

    public static class WPBPacketFactory extends PacketFactory {
        @Override
        public Packet getRequestPacket(Packet reusable, Request request) {
            return new SamplePacket(request.command(), request.body());
        }

        @Override
        public Packet getHeartBeatPacket(Packet recyclable) {
            return null;
        }

        @Override
        public Packet getRemotePacket(Packet reusable, BufferedSource source) throws IOException {
            return SamplePacket.build(reusable, source);
        }
    }
}
