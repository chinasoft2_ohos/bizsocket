package bizsocket.core;

import bizsocket.tcp.Packet;

/**
 * Interceptor
 *
 * @since 2021-05-06
 */
public interface Interceptor {
    /**
     * 决定是否拦截请求
     *
     * @param context
     * @return true 拦截请求
     * @throws Exception
     */
    boolean postRequestHandle(RequestContext context) throws Exception;

    /**
     * 决定是否拦截响应
     *
     * @param command
     * @param responsePacket
     * @return true 拦截响应
     * @throws Exception
     */
    boolean postResponseHandle(int command, Packet responsePacket) throws Exception;
}
