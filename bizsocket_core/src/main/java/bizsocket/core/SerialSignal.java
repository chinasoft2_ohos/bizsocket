package bizsocket.core;

import java.util.Arrays;

/**
 * SerialSignal
 *
 * @since 2021-05-06
 */
public class SerialSignal {
    /**
     * 必须等待的命令，需要是有序的
     */
    public static final int FLAG_ORDERED = 0x1;

    private int entranceCommand;

//    public void setSpStrong(int[] spStrong) {
//        this.spStrong = spStrong;
//    }
//
//    public void setSpSoft(int[] spSoft) {
//        this.spSoft = spSoft;
//    }

    private int[] spStrong;

    private int[] spSoft;

    private int flags;
    private Class<? extends AbstractSerialContext> serialContextType;

    public int getEntranceCommand() {
        return entranceCommand;
    }

    public SerialSignal(Class<? extends AbstractSerialContext> serialContextType, int entranceCommand, int[] strongReferences) {
        this(serialContextType, entranceCommand, strongReferences, null);
    }

    public SerialSignal(Class<? extends AbstractSerialContext> serialContextType, int entranceCommand, int[] spStrong, int[] spSoft) {
        this.serialContextType = serialContextType;
        this.entranceCommand = entranceCommand;
    }

    private int[] getStrongReferences() {
        return spStrong;
    }


    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public boolean isStrongReference(int command) {
        if (spStrong != null) {
            for (int cc : spStrong) {
                if (command == cc) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isWeekReference(int command) {
        if (spSoft != null) {
            for (int cc : spSoft) {
                if (command == cc) {
                    return true;
                }
            }
        }
        return false;
    }

    public Class<? extends AbstractSerialContext> getSerialContextType() {
        return serialContextType;
    }

    @Override
    public String toString() {
        return "SerialSignal{"
                +
                "entranceCommand=" + entranceCommand
                +
                ", strongReferences=" + Arrays.toString(spStrong)
                +
                ", weekReferences=" + Arrays.toString(spSoft)
                +
                '}';
    }
}

