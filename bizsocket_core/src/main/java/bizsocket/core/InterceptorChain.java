package bizsocket.core;

import bizsocket.tcp.LoggerUtil;
import bizsocket.tcp.Packet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * InterceptorChain
 *
 * @since 2021-05-06
 */
public class InterceptorChain {
    private final List<Interceptor> interceptors = Collections.synchronizedList(new ArrayList<Interceptor>());

    public void addInterceptor(Interceptor interceptor) {
        if (!interceptors.contains(interceptor)) {
            interceptors.add(interceptor);
        }
    }

    public void removeInterceptor(Interceptor interceptor) {
        interceptors.remove(interceptor);
    }

    public boolean invokePostRequestHandle(RequestContext context) {
        for (Interceptor interceptor : interceptors) {
            LoggerUtil.i("拦截器---拦截请求日志invokePostRequestHandle");
            try {
                if (interceptor.postRequestHandle(context)) {

                    //拦截请求
                    LoggerUtil.i("拦截器---拦截请求");
                    return true;
                }
            } catch (Exception e) {
                e.fillInStackTrace();
            }
        }
        return false;
    }

    public boolean invokePesponseHandle(int command, Packet responsePacket) {
        for (Interceptor interceptor : interceptors) {
            try {
                LoggerUtil.i("拦截器---拦截响应日志invokePesponseHandle");
                if (interceptor.postResponseHandle(command, responsePacket)) {

                    //拦截响应
                    LoggerUtil.i("拦截器---拦截响应");

                    return true;
                }
            } catch (Exception e) {
                e.fillInStackTrace();
            }
        }
        return false;
    }
}
