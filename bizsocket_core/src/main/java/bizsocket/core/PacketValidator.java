package bizsocket.core;

import bizsocket.tcp.Packet;

/**
 * PacketValidator
 *
 * @since 2021-05-06
 */
public interface PacketValidator {
    /**
     * 校验包的有效性
     *
     * @param packet
     * @return true 有效  false 无效
     */
    boolean verify(Packet packet);
}
