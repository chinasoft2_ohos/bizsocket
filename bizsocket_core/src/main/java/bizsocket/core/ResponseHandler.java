package bizsocket.core;

import bizsocket.tcp.Packet;
import okio.ByteString;

/**
 * ResponseHandler
 *
 * @since 2021-05-06
 */
public interface ResponseHandler {
    /**
     * Notifies callback, that request was handled successfully
     *
     * @param command    command code
     * @param requestBody
     * @param responsePacket
     */
    void sendSuccessMessage(int command, ByteString requestBody, Packet responsePacket);

    /**
     * Returns if request was completed with error code or failure of implementation
     *
     * @param command command code
     * @param error cause of request failure
     */
    void sendFailureMessage(int command, Throwable error);
}
