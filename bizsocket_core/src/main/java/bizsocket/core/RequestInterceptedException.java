package bizsocket.core;

/**
 * RequestInterceptedException
 *
 * @since 2021-05-06
 */
public class RequestInterceptedException extends RuntimeException {
    public RequestInterceptedException() {
    }

    public RequestInterceptedException(String detailMessage) {
        super(detailMessage);
    }
}
