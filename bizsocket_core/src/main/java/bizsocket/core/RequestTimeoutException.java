package bizsocket.core;

import java.net.SocketTimeoutException;

/**
 * RequestTimeoutException
 *
 * @since 2021-05-06
 */
public class RequestTimeoutException extends SocketTimeoutException {
    public RequestTimeoutException() {
    }

    public RequestTimeoutException(String detailMessage) {
        super(detailMessage);
    }
}
