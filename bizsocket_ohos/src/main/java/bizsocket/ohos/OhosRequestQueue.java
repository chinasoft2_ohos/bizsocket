package bizsocket.ohos;

import bizsocket.core.AbstractBizSocket;
import bizsocket.core.RequestQueue;
import bizsocket.tcp.Packet;
import ohos.eventhandler.EventRunner;

/**
 * OhosRequestQueue
 *
 * @since 2021-05-06
 */
public class OhosRequestQueue extends RequestQueue {

    public OhosRequestQueue(AbstractBizSocket bizSocket) {
        super(bizSocket);
    }

    @Override
    public void dispatchPacket(final Packet responsePacket) {
        if (EventRunner.getMainEventRunner().getThreadId() == Thread.currentThread().getId()) {
            OhosRequestQueue.super.dispatchPacket(responsePacket);
        } else {
            Util.runInMainThread(new Runnable() {
                @Override
                public void run() {
                    OhosRequestQueue.super.dispatchPacket(responsePacket);
                }
            });

        }
    }
}
