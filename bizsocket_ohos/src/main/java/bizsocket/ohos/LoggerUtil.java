package bizsocket.ohos;

/**
 * LoggerUtil
 *
 * @since 2021-05-06
 */
public class LoggerUtil {
    /**
     * 打印数据信息
     *
     * @param value
     */
    public static void i(String value) {
        System.out.println("hisocket--->       " + value);
    }
}
