package bizsocket.ohos;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.utils.IntervalTimer;

/**
 * Util
 *
 * @since 2021-05-06
 */
public class Util {
    private IntervalTimer gameTimer;
    private int i = 0;

    public static void runInMainThread(Runnable task) {
        EventRunner runner = EventRunner.getMainEventRunner();
        EventHandler handler = new EventHandler(runner);
        handler.postTask(task);
    }

    private void startGameLoop() {
        if (gameTimer == null) {
            gameTimer = new IntervalTimer(1000, 1000) {
                @Override
                public void onInterval(long l) {
                    runInMainThread(new Runnable() {
                        @Override
                        public void run() {
                            i++;
                        }
                    });
                }

                @Override
                public void onFinish() {
                    gameTimer.schedule();
                }
            };
            gameTimer.schedule();
        }
    }

    private void stopGameLoop() {
        if (gameTimer != null) {
            gameTimer.cancel();
            gameTimer = null;
        }
    }
}
