package bizsocket.ohos;

import bizsocket.core.*;
import bizsocket.logger.LoggerFactory;
import bizsocket.tcp.Packet;
import bizsocket.tcp.Request;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * OhosBizSocket
 *
 * @since 2021-05-06
 */
public abstract class OhosBizSocket extends AbstractBizSocket {
    private final EventRunner runner;
    private final EventHandler handler;

    static {
        LoggerFactory.setDefaultLoggerType(OhosLogger.class);
    }

    public OhosBizSocket() {
        runner = EventRunner.getMainEventRunner();
        handler = new EventHandler(runner);
    }

    public OhosBizSocket(Configuration configuration) {
        super(configuration);
        runner = EventRunner.getMainEventRunner();
        handler = new EventHandler(runner);
    }

    @Override
    public void setConfiguration(Configuration configuration) {
        super.setConfiguration(configuration);
    }

    @Override
    public RequestQueue createRequestQueue(AbstractBizSocket bizSocket) {
        return new OhosRequestQueue(bizSocket);
    }

    @Override
    protected RequestContext obtainRequestContext(Request request, Packet requestPacket, ResponseHandler responseHandler) {
        return new OhosRequestContext(request, requestPacket, responseHandler, handler);
    }
}
