package bizsocket.ohos;

import bizsocket.core.RequestContext;
import bizsocket.core.ResponseHandler;
import bizsocket.tcp.Packet;
import bizsocket.tcp.Request;
import ohos.eventhandler.EventHandler;

/**
 * OhosRequestContext
 *
 * @since 2021-05-06
 */
public class OhosRequestContext extends RequestContext {
    private final EventHandler handler;

    private final Runnable timeoutRunnable = new Runnable() {
        @Override
        public void run() {
            callRequestTimeout();
        }
    };

    public OhosRequestContext(Request request, Packet requestPacket, ResponseHandler responseHandler, EventHandler handler) {
        super(request, requestPacket, responseHandler);
        this.handler = handler;
    }

    @Override
    public void startTimeoutTimer() {
        Util.runInMainThread(timeoutRunnable);
    }

    @Override
    public void onRemoveFromQueue() {
        logger.debug("remove from queue: " + toString());
        handler.removeTask(timeoutRunnable);
    }
}
