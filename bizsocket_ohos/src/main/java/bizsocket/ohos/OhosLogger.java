package bizsocket.ohos;

import bizsocket.logger.Logger;

/**
 * OhosLogger
 *
 * @since 2021-05-06
 */
public class OhosLogger extends Logger {
    private static boolean LOG_ENABLE = true;

    public OhosLogger(String tag) {
        super(tag);
    }

    @Override
    public boolean isEnable() {
        return LOG_ENABLE;
    }

    @Override
    public void debug(String msg) {
        if (!LOG_ENABLE) {
            return;
        }
        LoggerUtil.i(msg);
    }

    @Override
    public void info(String msg) {
        if (!LOG_ENABLE) {
            return;
        }
        LoggerUtil.i(msg);
    }

    @Override
    public void warn(String msg) {
        if (!LOG_ENABLE) {
            return;
        }
        LoggerUtil.i(msg);
    }

    @Override
    public void error(String msg) {
        if (!LOG_ENABLE) {
            return;
        }
        LoggerUtil.i(msg);
    }
}
