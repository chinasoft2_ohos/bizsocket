package bizsocket.logger;

import java.lang.reflect.InvocationTargetException;

/**
 * LoggerFactory
 *
 * @since 2021-05-06
 */
public class LoggerFactory {
    private static Class<? extends Logger> defaultLoggerType;

    public static void setDefaultLoggerType(Class<? extends Logger> defaultLoggerType) {
        LoggerFactory.defaultLoggerType = defaultLoggerType;
    }

    public static Class<? extends Logger> getDefaultLoggerType() {
        return defaultLoggerType == null ? SystemOutLogger.class : defaultLoggerType;
    }

    public static Logger getLogger(String tags) {
        if (tags == null) {
            tags = "BizSocket";
        }
        return new SystemOutLogger(tags);
    }
}
