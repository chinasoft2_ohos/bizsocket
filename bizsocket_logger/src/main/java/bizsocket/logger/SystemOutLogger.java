package bizsocket.logger;

/**
 * SystemOutLogger
 *
 * @since 2021-05-06
 */
public class SystemOutLogger extends Logger {
    public SystemOutLogger(String tag) {
        super(tag);
    }

    @Override
    public boolean isEnable() {
        return true;
    }

    @Override
    public void debug(String msg) {
        LoggerUtil.i(tag + ": " + msg);
    }

    @Override
    public void info(String msg) {
        LoggerUtil.i(tag + ": " + msg);
    }

    @Override
    public void warn(String msg) {
        LoggerUtil.i(tag + ": " + msg);
    }

    @Override
    public void error(String msg) {
        LoggerUtil.i(tag + ": " + msg);
    }
}
