package bizsocket.logger;

/**
 * Logger
 *
 * @since 2021-05-06
 */
public abstract class Logger {
    public String tag;

    public Logger(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public abstract boolean isEnable();

    public abstract void debug(String msg);

    public abstract void info(String msg);

    public abstract void warn(String msg);

    public abstract void error(String msg);
}
